import csv
import json
import sys


def maybeFloat(value):
    try:
        return float(value)
    except ValueError:
        return None


def to_dict(fields, data):
    return {k: v for k, v in zip(fields, map(maybeFloat, data))}


def main():
    reader = csv.reader(sys.stdin)
    fields = reader.next()

    data = []

    for row in reader:
        data.append(to_dict(fields, row))

    print(json.dumps(data))

if __name__ == "__main__":
    sys.exit(main())
