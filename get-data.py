import csv
import math
import sys

import girder_client


def mean(vals):
    return sum(vals) / float(len(vals))


def avg(x, y):
    return 0.5 * (x + y)


def five_number_summary(raw_samples):
    samples = sorted(raw_samples)

    samp_min = samples[0]
    samp_max = samples[-1]

    N = len(samples)
    median_point = (N - 1) / 2.
    quarter_point = (N - 1) / 4.

    if int(median_point) == median_point:
        samp_med = samples[int(median_point)]
    else:
        lower = int(math.floor(median_point))
        upper = int(math.ceil(median_point))
        samp_med = avg(samples[lower], samples[upper])

    if int(quarter_point) == quarter_point:
        lower_quartile = samples[int(quarter_point)]
        upper_quartile = samples[-int(quarter_point + 1)]
    else:
        lower = int(math.floor(quarter_point))
        upper = int(math.ceil(quarter_point))

        lower_quartile = avg(samples[lower], samples[upper])
        upper_quartile = avg(samples[-(upper + 1)], samples[-(lower + 1)])

    return [samp_min, lower_quartile, samp_med, upper_quartile, samp_max]


def maybeGet(d, *keys):
    for k in keys:
        d = d.get(k)
        if d is None:
            return None
    return d


def main():
    gc = girder_client.GirderClient(host="parakon", port=8001)
    try:
        # gc.authenticate(interactive=True)
        gc.authenticate("roni", "hellohello")
    except girder_client.AuthenticationError:
        print >>sys.stderr, "invalid username or password"
        return 1

    item = gc.getItem("54f76e590640fd75455ce336")
    name = item.get("name")
    perfdata = maybeGet(item, "meta", "detailed")

    if perfdata is None:
        print >>sys.stderr, "no data found"
        return 1

    with open(name + ".csv", "wb") as f:
        writer = csv.writer(f)

        fields = ["count",
                  "p_load_min", "p_load_lower_q", "p_load_med", "p_load_upper_q", "p_load_max", "p_load_mean",
                  "p_render_min", "p_render_lower_q", "p_render_med", "p_render_upper_q", "p_render_max", "p_render_mean",
                  "l_load_min", "l_load_lower_q", "l_load_med", "l_load_upper_q", "l_load_max", "l_load_mean",
                  "l_render_min", "l_render_lower_q", "l_render_med", "l_render_upper_q", "l_render_max", "l_render_mean"]
        writer.writerow(fields)
        for count in sorted(map(int, perfdata.keys())):
            data = perfdata[str(count)]
            count = [count]

            point_load_times = maybeGet(data, "pload", "times")
            if point_load_times is None:
                point_load_stats = [""] * 6
            else:
                point_load_stats = five_number_summary(point_load_times) + [mean(point_load_times)]

            line_load_times = maybeGet(data, "lload", "times")
            if line_load_times is None:
                line_load_stats = [""] * 6
            else:
                line_load_stats = five_number_summary(line_load_times) + [mean(line_load_times)]

            point_anim_times = maybeGet(data, "panim", "times")
            if point_anim_times is None:
                point_anim_stats = [""] * 6
            else:
                point_anim_stats = five_number_summary(point_anim_times) + [mean(point_anim_times)]

            line_anim_times = maybeGet(data, "lanim", "times")
            if line_anim_times is None:
                line_anim_stats = [""] * 6
            else:
                line_anim_stats = five_number_summary(line_anim_times) + [mean(line_anim_times)]

            row = count + point_load_stats + point_anim_stats + line_load_stats + line_anim_stats

            writer.writerow(row)

    return 0

if __name__ == "__main__":
    sys.exit(main())
